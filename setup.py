# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function, absolute_import

import macchiato

from distutils.core import setup

version = '1.0.3'


description = macchiato.__doc__
long_description = '''Macchiato is a simple gui frontend for cups printer
settings.

Particular focus is set on configuring printer options, selecting a default
printer and administrating printer profiles.

While being a user interface in the first instance, it also provides a
suitable application logic library.'''


settings = dict(

    name = 'macchiato',
    version = version,

    packages = [b'macchiato'],
    requires = ['pycups'],
    scripts = ['bin/macchiato'],

    author = 'Florian Haftmann',
    author_email = 'florian.haftmann@informatik.tu-muenchen.de',
    description = description,
    long_description = long_description,

    classifiers = [
      'Development Status :: 1 - Planning',
      'Environment :: X11 Applications :: Qt',
      'Intended Audience :: End Users/Desktop',
      'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
      'Operating System :: Unix',
      'Programming Language :: Python :: 2',
      'Topic :: Printing'
    ],
    url = 'http://bitbucket.org/haftmann/macchiato'

)

setup(**settings)
