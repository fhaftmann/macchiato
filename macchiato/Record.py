# -*- coding: utf-8 -*-

'''
    Record types as syntactic sugar for collections.namedtuple.
'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)

from __future__ import absolute_import, division, print_function, \
  unicode_literals


__all__ = ['Record']


from collections import namedtuple


def Record(*fields):

    class Record(object):

        class __metaclass__(type):

            def __new__(Meta_Class, name, bases, dict):

                if name == 'Record' and bases == (object,):
                    return type.__new__(Meta_Class, name, bases, dict)
                else:
                    record = namedtuple(name, fields)
                    @property
                    def __mapping__(self):
                        for field in fields:
                            yield field, getattr(self, field)
                    record.__mapping__ = __mapping__
                    return record

    return Record
