# -*- coding: utf-8 -*-

'''
    Graphical user interface.
'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)

from __future__ import absolute_import, division, print_function, \
  unicode_literals

__all__ = ['Gui']

from PyQt5.QtCore import Qt, QLocale, QLibraryInfo, QTranslator
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtWidgets import QApplication, QDialog, QPushButton, \
  QHBoxLayout, QVBoxLayout, QFormLayout, QStackedLayout, \
  QScrollArea, QWidget, QComboBox, QMessageBox, QLabel, \
  QInputDialog, QLineEdit

from .guarded_main_loop import guarded_main_loop
from . import printers
from .printers import Printer
from .Printer_Mapping import Printer_Mapping


app_name = __name__.split('.')[0]
app_title = 'Druckereinstellungen'
icon_device = QIcon.fromTheme('printer')
icon_profile = QIcon.fromTheme('document-edit')

# Whether or not to hide "Installable options" (like optional additional
# paper trays, memory modules, finishers, etc.).
# Whether or not these are installed is not user-specific and is possibly
# better handled in the system-wide settings of the printer.
# For a more detailed description of "Installable Options",
# s. the PPD File Format Specification, section "5.4 Installable Options"
# (as of version 4.3 of the specification).
hide_installable_options = True

# keyword to denote the installable options group (s. PPD specification)
ppd_keyword_installable_options = 'InstallableOptions'

err_message_printer_processing = 'Bei der Verarbeitung des Druckers (bzw. ' \
    'Druckerprofils) "{}" ist ein Fehler aufgetreten.\nDer Drucker (bzw. das ' \
    'Druckerprofil) wird daher nicht angezeigt.\n\nMöglicherweise ist die ' \
    'PPD-Datei des Druckers fehlerhaft.'


def label(txt):

    label = QLabel(txt)
    label.setTextFormat(Qt.PlainText)
    return label


def button(txt, key):

    button = QPushButton()
    button.setText(txt)
    if key:
        button.setShortcut(QKeySequence(key))
    return button


def show_warning(message):
    msg_box = QMessageBox(QMessageBox.Warning, 'Warnung',
        message, QMessageBox.Ok)
    msg_box.exec_()


class Printer_Option(QComboBox):

    def __init__(self, connect, parent, choices, selected):

        QComboBox.__init__(self, parent)
        for _, choice_text in choices.iteritems():
            self.addItem(choice_text)
        self.setCurrentIndex(selected)
        self.original_index = selected
        self.pending_change = None
        self.choice_names = choices.keys()

        @connect(self.currentIndexChanged)
        def current_index_changed(index):
            self.pending_change = index

    def change(self):

        return self.choice_names[self.pending_change] \
          if self.pending_change is not None else None

    def commit(self):

        if self.pending_change is not None:
            self.original_index = self.pending_change
            self.pending_change = None

    def discard(self):

        if self.pending_change is not None:
            self.setCurrentIndex(self.original_index)
            self.pending_change = None


class Printer_Options(QWidget):

    def __init__(self, connect, groups):

        QWidget.__init__(self)
        layout = QHBoxLayout(self)
        area = QScrollArea(self)

        form_panel = QWidget()
        form_layout = QFormLayout(form_panel)
        self.printer_options = {}

        if (hide_installable_options):
            groups.pop(ppd_keyword_installable_options, None)

        for group_name, options in printers.flat_groups(groups):
            if group_name:
                heading = label(group_name)
                heading.setAlignment(Qt.AlignHCenter)
                form_layout.addRow(heading)
            for option_name, option in options.iteritems():
                printer_option = Printer_Option(connect, form_panel, option.choices, option.selected)
                form_layout.addRow(label(option.text), printer_option)
                self.printer_options[option_name] = printer_option
        area.setWidget(form_panel)

        area.adjustSize()
        layout.addWidget(area)

    def changes(self):

        for option_name in self.printer_options:
            value = self.printer_options[option_name].change()
            if value is not None:
                yield option_name, value

    def commit(self):

        for printer_option in self.printer_options.values():
            printer_option.commit()

    def discard(self):

        for printer_option in self.printer_options.values():
            printer_option.discard()


class Printer_Options_All(QStackedLayout):

    def __init__(self, connect, options):

        QStackedLayout.__init__(self)
        self.connect = connect
        self.panels = []
        for device in options:
            for profile in options[device]:
                try:
                    panel = Printer_Options(connect, options[device][profile])
                    self.panels.append(panel)
                    self.addWidget(panel)

                except:
                    printer_text = printers.text_of_printer(Printer(device, profile))
                    show_warning(err_message_printer_processing.format(printer_text))

                    # exclude this printer profile from further processing
                    options[device].pop(profile)

    def __getitem__(self, printer_index):

        return self.panels[printer_index]

    def switch(self, printer_index):

        self.setCurrentIndex(printer_index)

    def reload(self, printer_index, groups):

        panel = Printer_Options(self.connect, groups)
        self.panels[printer_index] = panel
        self.takeAt(printer_index)
        self.insertWidget(printer_index, panel)
        self.setCurrentIndex(printer_index)

    def insert(self, printer_index, groups):

        panel = Printer_Options(self.connect, groups)
        self.panels.insert(printer_index, panel)
        self.insertWidget(printer_index, panel)

    def remove(self, printer_index):

        self.panels.pop(printer_index)
        self.takeAt(printer_index)


class Macchiato(QDialog):

    def __init__(self, connect):

        QDialog.__init__(self)
        self.setWindowTitle(app_title)

        options, default, failed_printers = printers.get_settings()

        for printer in failed_printers:
            show_warning(err_message_printer_processing.format(
                printers.text_of_printer(printer)))

        layout_main = QVBoxLayout(self)

        self.printer_selector = QComboBox()
        # Create GUI elements for printers/profiles
        # If exception occurs for a specific printer (profile), it is removed
        self.layout_options = Printer_Options_All(connect, options)

        self.printer_mapping = Printer_Mapping(options)
        self.default_index = self.printer_mapping.index_of(
            default) if self.printer_mapping.exists(default) else None

        for device in options:
            for profile in options[device]:
                self.printer_selector.addItem(icon_device if profile is None else icon_profile,
                  printers.text_of_printer(Printer(device, profile)))
        if self.default_index is not None:
            self.printer_selector.setCurrentIndex(self.default_index)
        layout_main.addWidget(self.printer_selector)

        layout_settings = QHBoxLayout()

        layout_options_config = QVBoxLayout()

        layout_options_config.addLayout(self.layout_options)

        layout_options_buttons = QHBoxLayout()
        apply = button('&Anwenden', 'Alt+A')
        layout_options_buttons.addWidget(apply)
        discard = button('&Verwerfen', 'Alt+V')
        layout_options_buttons.addWidget(discard)
        reset = button('V&oreinstellung', 'Alt+O')
        layout_options_buttons.addWidget(reset)
        layout_options_config.addLayout(layout_options_buttons)

        layout_settings.addLayout(layout_options_config)

        layout_printers = QVBoxLayout()
        self.set_default = button('Als Standard&drucker setzen', 'Alt+D')
        layout_printers.addWidget(self.set_default)
        self.create = button('Profil &erstellen', 'Alt+E')
        layout_printers.addWidget(self.create)
        self.rename = button('Profil &umbenennen', 'Alt+U')
        layout_printers.addWidget(self.rename)
        self.delete = button('Profil ent&fernen', 'Alt+F')
        layout_printers.addWidget(self.delete)
        layout_printers.insertStretch(-1, 1)
        close = button('&Schließen', 'Alt+S')
        layout_printers.addWidget(close)

        layout_settings.addLayout(layout_printers)

        layout_main.addLayout(layout_settings)

        def ask_for_profile_name(title, device, old_name):

            while True:
                new, is_ok = QInputDialog.getText(self, title,
                   'Neuer Name\n(Name darf alle Zeichen außer Leerzeichen, "/" und "#" enthalten):',
                   QLineEdit.Normal, old_name)
                new = unicode(new)
                if not is_ok or new == old_name:
                    return None
                if self.printer_mapping.exists(Printer(device, new)):
                    QMessageBox.warning(self, title,
                      'Ein Profil namens »{}« existiert bereits für Drucker »{}.«'.format(new, device),
                      QMessageBox.Retry)
                elif not printers.check_profile_name(new):
                    QMessageBox.warning(self, title, 'Ungültiger Profilname: »{}«'.format(new),
                      QMessageBox.Retry)
                elif new != '':
                    return new

        @connect(self.printer_selector.currentIndexChanged)
        def printer_selector_currentIndexChanged(index):
            self.update_display()

        @connect(self.set_default.clicked)
        def set_default_clicked(checked):
            self.default_index = self.current_printer_index()
            printers.set_default(self.printer_mapping.printer_for(self.default_index))
            self.update_display()

        @connect(self.create.clicked)
        def create_clicked(checked):
            printer_index = self.current_printer_index()
            printer = self.printer_mapping.printer_for(printer_index)
            title = 'Profil erstellen'
            new = ask_for_profile_name(title, printer.device, '')
            if new is None:
                return
            printer_index += 1
            printer = Printer(printer.device, new)
            printers.create_profile(printer.device, printer.profile)
            groups = printers.get_printer_options(printer)
            self.printer_mapping.create_profile(printer)
            self.printer_selector.insertItem(printer_index, icon_profile,
              printers.text_of_printer(printer))
            self.layout_options.insert(printer_index, groups)
            self.printer_selector.setCurrentIndex(printer_index)

            self.update_display()

        @connect(self.rename.clicked)
        def rename_clicked(checked):
            printer_index = self.current_printer_index()
            printer = self.printer_mapping.printer_for(printer_index)
            title = 'Profil umbenennen'
            new = ask_for_profile_name(title, printer.device, printer.profile)
            if new is None:
                return
            printers.move_profile(printer.device, printer.profile, new)
            self.printer_mapping.rename_profile(printer.device, printer.profile, new)
            self.printer_selector.setItemText(printer_index,
              printers.text_of_printer(Printer(printer.device, new)))
            self.update_display()

        @connect(self.delete.clicked)
        def delete_clicked(checked):
            printer_index = self.current_printer_index()
            printer = self.printer_mapping.printer_for(printer_index)
            printers.remove_profile(printer)
            self.printer_mapping.remove_profile(printer)
            self.printer_selector.removeItem(printer_index)
            self.layout_options.remove(printer_index)
            self.printer_selector.setCurrentIndex(
              self.printer_mapping.index_of(Printer(printer.device, None)))
            default = printers.get_default_printer()
            self.default_index = self.printer_mapping.index_of(default)
            self.update_display()

        @connect(apply.clicked)
        def apply_clicked(checked):
            current_options = self.current_options()
            changes = current_options.changes()
            printers.set_options(self.printer_mapping.printer_for(self.current_printer_index()), dict(changes))
            current_options.commit()

        @connect(discard.clicked)
        def discard_clicked(checked):
            self.current_options().discard()

        @connect(reset.clicked)
        def reset_clicked(checked):
            printer_index = self.current_printer_index()
            printer = self.printer_mapping.printer_for(printer_index)
            printers.del_all_options(printer)
            groups = printers.get_printer_options(printer)
            self.layout_options.reload(printer_index, groups)

        @connect(close.clicked)
        def close_clicked(checked):
            self.accept()

        if self.printer_mapping:
            if self.default_index:
                self.printer_selector.setCurrentIndex(self.default_index)
            self.update_display()
        else:
            self.set_default.setDisabled(True)
            apply.setDisabled(True)
            discard.setDisabled(True)

    def current_printer_index(self):

        return self.printer_selector.currentIndex()

    def current_options(self):

        return self.layout_options[self.current_printer_index()]

    def update_display(self):

        self.current_options().discard()
        printer_index = self.current_printer_index()
        self.layout_options.switch(printer_index)
        self.set_default.setDisabled(printer_index == self.default_index)
        is_profile = self.printer_mapping.printer_for(printer_index).profile is not None
        self.create.setDisabled(is_profile)
        self.rename.setEnabled(is_profile)
        self.delete.setEnabled(is_profile)


class Gui(object):

    def __init__(self):

        self.qapp = QApplication([app_name])
        self.qapp.setWindowIcon(icon_device)

        self.translator = QTranslator()
        self.translator.load('qt_' + QLocale.system().name(), QLibraryInfo.location(QLibraryInfo.TranslationsPath))
        self.qapp.installTranslator(self.translator)
          # bind translator object as attribute to prevent garbage collection of translator
          # -- a »feature« of PyQt

    @classmethod
    def main(Class):

        gui = Class()
        try:
            def main_loop(connect):
                dlg = Macchiato(connect)
                dlg.show()
                gui.qapp.exec_()
            guarded_main_loop(gui.qapp, main_loop)
        except Exception:
            msg_box = QMessageBox(QMessageBox.Critical, 'Fehler',
              'Es ist ein Fehler aufgetreten. Beenden Sie die Anwendung und starten Sie sie neu.', QMessageBox.Abort)
            msg_box.exec_()
            raise
