# -*- coding: utf-8 -*-

'''
    Generic utilities.
'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)

from __future__ import absolute_import, division, print_function, \
  unicode_literals


__all__ = ['of_utf8', 'utf8_of']


strict = 'strict'


def of_utf8(s):

    assert isinstance(s, str)
    return unicode(s, b'utf-8', strict)


def utf8_of(txt):

    assert isinstance(txt, unicode)
    return txt.encode(b'utf-8', strict)
