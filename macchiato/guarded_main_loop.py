# -*- coding: utf-8 -*-

'''
    Guarded main loop: exceptions during event handlers are propagated to main loop.
'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)

from __future__ import absolute_import, division, print_function, \
  unicode_literals


__all__ = ['guarded_main_loop']


import functools


def guarded_main_loop(qapp, main_loop):

    pending_exception = [None]  ## destructive shared variable

    def connect(signal):

        def connect_(f):

            @functools.wraps(f)
            def f_(*args, **kwarg):
                try:
                    f(*args, **kwarg)
                except Exception, e:
                    pending_exception[0] = e
                    qapp.exit()

            signal.connect(f_)

        return connect_

    main_loop(connect)
    pending_exception = pending_exception[0]
    if pending_exception is not None:
        raise pending_exception
