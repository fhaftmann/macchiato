# -*- coding: utf-8 -*-

'''
    Printer data retrieval, representation and modification.
'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)

from __future__ import absolute_import, division, print_function, \
  unicode_literals

__all__ = ['Printer', 'Group', 'Option',
  'get_settings', 'get_printer_options', 'get_default_printer',
  'flat_groups', 'set_default', 'set_option', 'set_options',
  'del_option', 'del_all_options', 'check_profile_name',
  'create_profile', 'move_profile', 'remove_profile']

from collections import OrderedDict
import re
import os
from os import path
import io
import subprocess

import cups

from . import util
from .Record import Record


class Printer(Record('device', 'profile')): pass

class Option(Record('text', 'choices', 'selected')): pass

class Group(Record('text', 'groups', 'options')): pass


re_option = re.compile(R'([^/]+)/[^:]+:')
re_value = re.compile(R'\s*(\*)?(\S+)')
re_default = re.compile(R'Default\s+(\S+)')

warn_ppd_not_retrieved = 'Failed to retrieve PPD for {} - IPP error {}: {}'


def loc_lpoptions():

    return '/etc/cups/lpoptions' if os.getuid() == 0 \
      else path.join(path.expanduser('~'), '.cups/lpoptions')


def text_of_printer(printer):

    return printer.device if printer.profile is None \
      else printer.device + '/' + printer.profile


def printer_of_text(txt):

    txts = txt.split('/', 2)
    return Printer(device = txts[0], profile = txts[1] if len(txts) == 2 else None)


def get_default_printer():

    loc = loc_lpoptions()
    if path.isfile(loc):
        with io.open(loc, 'r') as reader:
            for line in reader:
                match = re_default.match(line)
                if match:
                    return printer_of_text(match.group(1))
    return None


def obtain_lpoptions(printer):

    s = subprocess.check_output(['lpoptions', '-p', text_of_printer(printer), '-l'])
    txt = util.of_utf8(s.rstrip())
    for line in txt.split('\n'):
        match = re_option.match(line)
        option = match.group(1)
        selected = None
        values = []
        for k, match in enumerate(re_value.finditer(line, match.end(0))):
            if match.group(1):
                selected = k
            values.append(match.group(2))
        yield option, values, selected


def get_printers(conn):

    for device, some_profile in sorted(conn.getDests()):
        if device is None:
            continue

        device = util.of_utf8(device)
        try:
            conn.getPPD(device)
        except cups.IPPError as (status_code, description):
            # skip devices which have no PPD or whose PPD cannot be retrieved
            print(warn_ppd_not_retrieved.format(device, status_code, description))
            continue
        yield (device,
          util.of_utf8(some_profile) if some_profile else None)


def obtain_printer_options(conn, printer):

    ppd = cups.PPD(conn.getPPD(printer.device))
    # use localized version of options, if available
    ppd.localize()

    settings = {}
    for option, values, selected in obtain_lpoptions(printer):
        settings[option] = values, selected

    def combine_option_with_settings(option):

        name = option.keyword
        if name not in settings:
            return name, None

        choices = OrderedDict()
        for choice in option.choices:
            choices[choice['choice']] = choice['text']
        selected = settings[name][1]
        return name, Option(text = option.text, choices = choices, selected = selected)

    def combine_group_with_settings(group):

        groups = OrderedDict()
        for group in group.subgroups:
            name, entry = combine_group_with_settings(group)
            groups[name] = entry

        options = OrderedDict()
        for option in group.options:
            name, some_entry = combine_option_with_settings(option)
            if some_entry is not None:
                options[name] = some_entry

        return group.name, Group(text = group.text, groups = groups, options = options)

    groups = OrderedDict()
    for group in ppd.optionGroups:
        name, entry = combine_group_with_settings(group)
        groups[name] = entry
    return groups


def flat_groups(groups):

    def flat(prefix, groups):

        for group in groups.values():
            group_prefix = prefix + [group.text]
            yield ' / '.join(group_prefix), group.options
            for entry in flat(group_prefix, group.groups):
                yield entry

    return flat([], groups)


def get_settings():

    conn = cups.Connection()

    printers = OrderedDict()
    for device, some_profile in get_printers(conn):
        printers.setdefault(device, []).append(some_profile)

    # printers whose options cannot be obtained (e.g. due to broken PPD file)
    failed_printers = []

    options = OrderedDict()
    for device in printers:
        printer_options = OrderedDict()
        for some_profile in printers[device]:
            try:
                printer_options[some_profile] = obtain_printer_options(conn, Printer(device, some_profile))
            except:
                failed_printers.append(Printer(device, some_profile))
        options[device] = printer_options

    default = get_default_printer()

    # check whether user's default printer actually exists
    if default and default.device not in printers:
        default = None

    return options, default, failed_printers


def get_printer_options(printer):

    conn = cups.Connection()
    return obtain_printer_options(conn, printer)


def set_default(printer):

    _ = subprocess.check_output(['lpoptions', '-d', text_of_printer(printer)])


def set_option(printer, option, choice):

    subprocess.check_call(['lpoptions', '-p', text_of_printer(printer), '-o', option + '=' + choice])


def set_options(printer, options):

    if options:
        subprocess.check_call(['lpoptions', '-p', text_of_printer(printer)] +
          [arg for args in [['-o', name + '=' + options[name]] for name in options] for arg in args])


def del_option(printer, option):

    subprocess.check_call(['lpoptions', '-p', text_of_printer(printer), '-r', option])


def del_all_options(printer):

    subprocess.check_call(['lpoptions', '-x', text_of_printer(printer)])


def check_profile_name(profile):

    return all(c not in ' /#' for c in profile)


def copy_settings(old, new):

    groups = get_printer_options(old)
    subprocess.check_call(['lpoptions', '-p', text_of_printer(new)] +
      [arg for args in [['-o', name + '=' + list(option.choices)[option.selected]]
      for _, options in flat_groups(groups) for name, option in options.iteritems()] for arg in args]
    )


def delete_printer(printer):

    subprocess.check_call(['lpoptions', '-x', text_of_printer(printer)])


def create_profile(device, profile_name):

    copy_settings(Printer(device, None), Printer(device, profile_name))


def remove_profile(printer):

    if get_default_printer() == printer:
        set_default(Printer(printer.device, None))
    delete_printer(printer)


def move_profile(device, old, new):

    printer_old = Printer(device, old)
    printer_new = Printer(device, new)

    is_default = get_default_printer() == printer_old
    copy_settings(printer_old, printer_new)
    if is_default:
        set_default(printer_new)

    delete_printer(printer_old)
