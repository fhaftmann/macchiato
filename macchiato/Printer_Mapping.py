# -*- coding: utf-8 -*-

'''
    Auxiliary mapping for sequential indexing of printers and profiles.
'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)

from __future__ import absolute_import, division, print_function, \
  unicode_literals


__all__ = ['Printer_Mapping']


from .printers import Printer


class Printer_Mapping(object):

    def __init__(self, options):

        self.mapping = [Printer(device, profile)
          for device in options for profile in options[device]]

    def exists(self, printer):

        return printer in self.mapping

    def printer_for(self, printer_index):

        return self.mapping[printer_index]

    def index_of(self, printer):

        return self.mapping.index(printer)

    def create_profile(self, printer):

        self.mapping.insert(self.index_of(Printer(printer.device, None)) + 1,
          printer)

    def rename_profile(self, device, old, new):

        self.mapping = [Printer(printer.device,
          new if printer.device == device and printer.profile == old else printer.profile)
          for printer in self.mapping]

    def remove_profile(self, printer):

        self.mapping.remove(printer)
