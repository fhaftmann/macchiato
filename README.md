# README #

### What is this repository for? ###

Macchiato is a simple gui frontend for cups printer
settings.

Particular focus is set on configuring printer options, selecting a default
printer and administrating printer profiles.

While being a user interface in the first instance, it also provides a
suitable application logic library.
